﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CompleteSolution.Models;
using System.Diagnostics;
using Silanis.ESL.SDK;

namespace CompleteSolution.Controllers
{
    public class AutoSubmitController : Controller
    {
        public ActionResult Index()
        {
            string status = Request.QueryString.Get("status");
            string signerId = Request.QueryString.Get("signer");
            string package = Request.QueryString.Get("package");


            Trace.WriteLine("Signer ID: " + signerId);
            PackageId packageId = new PackageId(package);
            NotificationEventsType type;
            ViewResult Congratulations = View("AutoSubmitOnComplete");
            ViewResult InterruptedSigning = View("AutoSubmitOnOther");

            if (Enum.TryParse(status, out type))
            {
                Trace.WriteLine("Status for redirection: " + status);
                if (type.Equals(NotificationEventsType.SIGNER_COMPLETE) || type.Equals(NotificationEventsType.PACKAGE_COMPLETE))
                {
                    //Displays the "Congratulations Page" of the insurance company web application 
                    return Congratulations;
                }
                else
                {
                    //Displays an "Interrupted Signing Page" for packages with statuses other than PACKAGE_COMPLETE and SIGNER_COMPLETE
                    return InterruptedSigning;
                }
            }
            else
            {
                throw new SystemException("Unhandled notification type " + status);
            }
        }
	}
}