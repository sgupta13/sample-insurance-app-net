﻿using System.Web;
using System.IO;
using System.Collections.Generic;
using System.Web.Mvc;
using CompleteSolution.Models;
using Silanis.ESL.SDK;
using Silanis.ESL.SDK.Builder;
using System;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using CompleteSolution.Controllers;
using CompleteSolution.Services;


namespace CompleteSolution.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            // Displays the "Welcome Page" of the insurance company web application where the user is promoted to start filling his insurance form
            return View();
        }

        public ActionResult Congratulations()
        {
            return View();
        }

        public ActionResult InterruptedSigning()
        {
            return View();
        }
    }
}

