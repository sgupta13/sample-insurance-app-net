﻿using Silanis.ESL.SDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CompleteSolution.Services;
using CompleteSolution.Models;

namespace CompleteSolution.Controllers
{
    public class InsuranceFormController : Controller
    {
        //
        // GET: /InsuranceForm/
        //Displays the "Insurance Form Page" of the insurance company web application  
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(PropertiesFile properties)
        {
            String API_KEY = properties.API_KEY;
            String BASE_URL = properties.BASE_URL;
            String AUTO_SUBMIT_URL = properties.AUTO_SUBMIT_URL;

            if (!string.IsNullOrEmpty(API_KEY))
            {
                HttpContext.Session["API_KEY"] = API_KEY;
            }

            if (!string.IsNullOrEmpty(BASE_URL))
            {
                HttpContext.Session["BASE_URL"] = BASE_URL;
            }

            if (!string.IsNullOrEmpty(AUTO_SUBMIT_URL))
            {
                HttpContext.Session["AUTO_SUBMIT_URL"] = AUTO_SUBMIT_URL;
            }

            if (String.IsNullOrEmpty(API_KEY) || String.IsNullOrEmpty(BASE_URL) || String.IsNullOrEmpty(AUTO_SUBMIT_URL))
            {
                throw new SystemException("Please fill in the properties file on the homepage. One or more entries are null");
            }
            return View();
        }

        [HttpPost]
        public ActionResult ESLSignature(InsuranceFormModel form)
        {
            string API_KEY =  (string) HttpContext.Session["API_KEY"];
            string BASE_URL = (string) HttpContext.Session["BASE_URL"];
            string AUTO_SUBMIT_URL = (string)HttpContext.Session["AUTO_SUBMIT_URL"];

            // Create a packageId 
            PackageId packageId = ESLPackageCreation.createPackage(form, API_KEY, BASE_URL, AUTO_SUBMIT_URL);

            //Get the session token 
            SessionToken token = ESLPackageCreation.createSessionToken(packageId, API_KEY, BASE_URL);

            ViewData["token"] = token.Token;

            // Redirect the signer to e-SignLive
            return View();
        }

	}
}