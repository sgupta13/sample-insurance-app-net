﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CompleteSolution.Services;
using System.IO;
using System.Diagnostics;

namespace CompleteSolution.Controllers
{
    public class DownloadDocumentsController : Controller
    {
        //
        // GET: /DownloadDocuments/
        //Displays the signed documents in a zip file for view or download
        public void Index()
        {
            String packageId = Request.QueryString["packageId"];
            string API_KEY = (string)HttpContext.Session["API_KEY"];
            string BASE_URL = (string)HttpContext.Session["BASE_URL"];

            if (string.IsNullOrEmpty(packageId))
            {
                throw new SystemException("packageId parameter is empty or null, please provide a valid package id");
            }

            DownloadDocumentsService svc = new DownloadDocumentsService();
            byte[] documents = svc.downloadZippedDocuments(packageId, API_KEY, BASE_URL);

            Trace.WriteLine("Documents .zip binary retrieved..");
            Trace.WriteLine("Length of byte array: " + documents.Length);

            Response.ContentType = "application/zip";
            Response.AppendHeader("Content-Length", documents.Length.ToString());
            Response.BinaryWrite(documents);
            Response.Flush();
            Response.End();
        }
	}
}