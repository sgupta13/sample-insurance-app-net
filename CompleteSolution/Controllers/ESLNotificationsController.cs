﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CompleteSolution.Models;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Diagnostics;
using CompleteSolution.Controllers;
using Silanis.ESL.SDK;
using Silanis.ESL.SDK.Builder;
using System.Text;
using CompleteSolution.Database;
using CompleteSolution.Services;

namespace CompleteSolution.Controllers
{
    public class ESLNotificationsController : Controller
    {
        static NotificationsDatabase database = NotificationsDatabase.getInstance;

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

  
        [HttpPost]
        public void Index(ESLNotificationsModel notification)
        {
            //TODO: Uncomment the logging statements below and deploy your application to the cloud and see it in action!
            Trace.WriteLine("Notification " + notification.name +  " for package " + notification.packageId + " received");
            
            //if(!String.IsNullOrEmpty(notification.message))
            Trace.WriteLine("Message: " + notification.message);

            //if(!String.IsNullOrEmpty(notification.documentId))
            Trace.WriteLine("Document ID: " + notification.documentId);

            //if (!String.IsNullOrEmpty(notification.sessionUser))
                Trace.WriteLine("Session user: " + notification.sessionUser);

            ESLNotificationsService svc = new ESLNotificationsService();
            svc.saveNotifications(notification);
            CompleteSolution.Models.NotificationEventsType enumName;

            if (Enum.TryParse(notification.name, out  enumName).Equals(NotificationEventsType.PACKAGE_COMPLETE))
            {
                Response.Redirect("/AutoSubmit/AutoSubmitOnComplete");
            }
            else
            {
                Response.ClearContent();
                Response.StatusCode = (int)HttpStatusCode.NoContent;
            } 
        }

        //Displays the e-SignLive event notifications
        //Displays the status of the packages
        [HttpGet]
        public ActionResult Dashboard()
        {
            Dictionary<String, String> updatedDatabase = database.getFromDatabase();
            if (updatedDatabase.Count == 0)
            {
                Trace.WriteLine("No packages to view..Please create a package.");
            }

            Trace.WriteLine("DB entries below:");
            foreach (var entry in updatedDatabase)
            {
                Trace.WriteLine("Package ID: " + entry.Key + " Status: " + entry.Value);
            }

            ViewData["updatedDatabase"] = updatedDatabase;
            return View();
        }
    }
}