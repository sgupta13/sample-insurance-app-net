﻿
namespace CompleteSolution.Models
{
    // Model of the properties form presented to the user
    public class PropertiesFile
    {
        
        public string API_KEY { get; set; }
        public string BASE_URL { get; set; }
        public string AUTO_SUBMIT_URL { get; set; }
     }

}