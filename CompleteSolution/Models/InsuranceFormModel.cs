﻿
namespace CompleteSolution.Models
{
    // Model of the insurance form presented to the user
    public class InsuranceFormModel
    {
        public string insuredInitials { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string province { get; set; }
        public string emailAddress { get; set; }
        public string language { get; set; }
        public string vehicleModel { get; set; }
        public string vehicleMake { get; set; }
        public string vehicleModelYear { get; set; }
        public string vehicleColor { get; set; }
    }

}