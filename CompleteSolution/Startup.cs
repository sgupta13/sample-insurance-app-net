﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CompleteSolution.Startup))]
namespace CompleteSolution
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
