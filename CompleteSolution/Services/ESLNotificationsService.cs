﻿using System;
using Silanis.ESL.SDK;
using System.Diagnostics;
using CompleteSolution.Models;
using CompleteSolution.Database;

namespace CompleteSolution.Services
{
    // Registers and saves the event notifications
    public class ESLNotificationsService
    {
        public void saveNotifications(ESLNotificationsModel eslNotification)
        {
            string name = eslNotification.name;
            string packageId = eslNotification.packageId;

            CompleteSolution.Models.NotificationEventsType enumName;
            NotificationsDatabase database = NotificationsDatabase.getInstance;

            if (Enum.TryParse(name, out enumName))
            {
                switch (enumName)
                {
                    case NotificationEventsType.PACKAGE_ACTIVATE: Trace.WriteLine("Package activated");
                        break;
                    case NotificationEventsType.PACKAGE_COMPLETE: Trace.WriteLine("Package completed");
                        break;
                    case NotificationEventsType.PACKAGE_CREATE: Trace.WriteLine("Package created");
                        break;
                    case NotificationEventsType.PACKAGE_DEACTIVATE: Trace.WriteLine("Package deactivated");
                        break;
                    case NotificationEventsType.PACKAGE_DECLINE: Trace.WriteLine("Package declined");
                        break;
                    case NotificationEventsType.PACKAGE_DELETE: Trace.WriteLine("Package deleted");
                        break;
                    case NotificationEventsType.PACKAGE_EXPIRE: Trace.WriteLine("Package expired");
                        break;
                    case NotificationEventsType.PACKAGE_OPT_OUT: Trace.WriteLine("Package opted out");
                        break;
                    case NotificationEventsType.PACKAGE_READY_FOR_COMPLETE: Trace.WriteLine("Package ready for completion");
                        break;
                    case NotificationEventsType.PACKAGE_RESTORE: Trace.WriteLine("Package restored");
                        break;
                    case NotificationEventsType.PACKAGE_TRASH: Trace.WriteLine("Package trashed");
                        break;
                    case NotificationEventsType.DOCUMENT_SIGNED: Trace.WriteLine("Document signed");
                        break;
                    case NotificationEventsType.ROLE_REASSIGN: Trace.WriteLine("Role reassigned activated");
                        break;
                    case NotificationEventsType.EMAIL_BOUNCE: Trace.WriteLine("Email bounced");
                        break;
                    case NotificationEventsType.KBA_FAILURE: Trace.WriteLine("KBA Failure");
                        break;
                    case NotificationEventsType.SIGNER_COMPLETE: Trace.WriteLine("Signer Complete");
                        break;
                    case NotificationEventsType.PACKAGE_ATTACHMENT: Trace.WriteLine("Package attachment uploaded");
                        break;
                    case NotificationEventsType.SIGNER_LOCKED: Trace.WriteLine("Signer locked");
                        break;
                }
                Trace.WriteLine("Saving to DB..");
                database.saveToDatabase(packageId, name);
            }
            else
            {
                throw new SystemException("Unhandled eslNotification type " + name);
            }
        }
    }

}