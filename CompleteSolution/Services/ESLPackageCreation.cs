﻿using System;
using Silanis.ESL.SDK;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using CompleteSolution.Models;
using Silanis.ESL.SDK.Builder;
using System.IO;
using System.Globalization;
using System.Web;
using CompleteSolution.Properties;

namespace CompleteSolution.Services
{
    //Package creation with e-SignLive
    public class ESLPackageCreation
    {
        readonly static string CUSTOM_ID = "BUYER";

        public static PackageId createPackage(InsuranceFormModel model, string API_KEY, string BASE_URL, string AUTO_SUBMIT_URL)
        {
            String insuredInitials = model.insuredInitials;
            String emailAddress = model.emailAddress;
            String firstName = model.firstName;
            String lastName = model.lastName;
            String language = model.language;
            String address = model.address;
            String city = model.city;
            String province = model.province;
            String vehicleModel = model.vehicleModel;
            String vehicleMake = model.vehicleMake;
            String vehicleModelYear = model.vehicleModelYear;
            String vehicleColor = model.vehicleColor;

            EslClient client = new EslClient(API_KEY, BASE_URL);

            Stream file = new MemoryStream(Properties.Resources.Insurance_Company_Contract_Final_Version);

            // Build the DocumentPackage object
            DocumentPackage documentPackage = PackageBuilder.NewPackageNamed("C# Insurance Policy Package " + DateTime.Now)

                // Customize package settings
                .WithSettings(DocumentPackageSettingsBuilder.NewDocumentPackageSettings()
                                .WithDecline()
                                .WithOptOut()
                                .WithDocumentToolbarDownloadButton()
              
                // Customize the HandOverLinkText and HandOverLinkTooltip
                                .WithHandOverLinkHref(AUTO_SUBMIT_URL)
                                .WithHandOverLinkText("Continue")
               
                                // Customize the signing ceremony settings
                                .WithCeremonyLayoutSettings(CeremonyLayoutSettingsBuilder.NewCeremonyLayoutSettings()
                                        .WithoutGlobalNavigation()
                                        .WithoutBreadCrumbs()
                                        .WithoutSessionBar()
                                        .WithTitle()))
                                        
                // Define the insured first and last name
                .WithSigner(SignerBuilder.NewSignerWithEmail(emailAddress)
                                         .WithCustomId(CUSTOM_ID)
                                         .WithFirstName(firstName)
                                         .WithLastName(lastName))

                // Define the document
                .WithDocument(DocumentBuilder.NewDocumentNamed("Insurance Form")
                                    .FromStream(file, DocumentType.PDF)
                                    .EnableExtraction()
                                    .WithSignature(SignatureBuilder.SignatureFor(emailAddress)
                                                                   .WithName("InsuredSignature")
                                                                   .WithPositionExtracted()                                                               

                                                                   .WithField(FieldBuilder.TextField()
                                                                                .WithPositionExtracted()
                                                                                .WithName("ExtraInfo"))

                                                                   // Bound fields
                                                                   .WithField(FieldBuilder.SignatureDate()
                                                                                .WithPositionExtracted()
                                                                                .WithName("Date"))

                                                                   .WithField(FieldBuilder.CheckBox()
                                                                                .WithPositionExtracted()
                                                                                .WithName("checkbox")))
                                   
                                     // Below are the form fields filled in by the Insured/customer
                                    .WithInjectedField(FieldBuilder.TextField()
                                                            .WithName("CustomerId")
                                                            .WithValue(insuredInitials))
                                    .WithInjectedField(FieldBuilder.TextField()
                                                            .WithName("FirstName")
                                                            .WithValue(firstName))
                                    .WithInjectedField(FieldBuilder.TextField()
                                                            .WithName("LastName")
                                                            .WithValue(lastName))
                                    .WithInjectedField(FieldBuilder.TextField()
                                                            .WithName("Address")
                                                            .WithValue(address))
                                    .WithInjectedField(FieldBuilder.TextField()
                                                            .WithName("City")
                                                            .WithValue(city))
                                    .WithInjectedField(FieldBuilder.TextField()
                                                            .WithName("Province")
                                                            .WithValue(province))
                                    .WithInjectedField(FieldBuilder.TextField()
                                                            .WithName("VehicleModel")
                                                            .WithValue(vehicleModel))
                                    .WithInjectedField(FieldBuilder.TextField()
                                                            .WithName("VehicleMake")
                                                            .WithValue(vehicleModel))
                                    .WithInjectedField(FieldBuilder.TextField()
                                                            .WithName("VehicleModelYear")
                                                            .WithValue(vehicleModelYear))
                                    .WithInjectedField(FieldBuilder.TextField()
                                                            .WithName("VehicleColor")
                                                            .WithValue(vehicleColor))

            ).Build();

            // Issue the request to the e-SignLive server to create the DocumentPackage
            PackageId packageId = client.CreatePackage(documentPackage);
            client.SendPackage(packageId);

            return packageId;
        }

        public static SessionToken createSessionToken(PackageId packageId, string API_KEY, string BASE_URL)
        {
            EslClient client = new EslClient(API_KEY, BASE_URL);

            SessionToken sessionToken = client.SessionService.CreateSessionToken(packageId, CUSTOM_ID);
            Trace.WriteLine("Session token: " + sessionToken.Token);

            return sessionToken;
        }
    }
}
