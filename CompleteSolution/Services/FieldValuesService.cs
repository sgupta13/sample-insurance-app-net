﻿using System;
using Silanis.ESL.SDK;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;


namespace CompleteSolution.Services
{
    //Retrieves the values found in the fields of each of the signed documents in a package
    public class FieldValuesService
    {
        public string getFieldValues(String packageId, string API_KEY, string BASE_URL)
        {
            PackageId pkgId = new PackageId(packageId);

            EslClient client = new EslClient(API_KEY, BASE_URL);
            Trace.WriteLine("eSL client for retrieving field values created..");

            List<FieldSummary> fieldSummaries = client.FieldSummaryService.GetFieldSummary(pkgId);

            Trace.WriteLine("SignerId, DocumentId, FieldId: Value");
            foreach (FieldSummary fieldSummary in fieldSummaries)
            {
                Trace.WriteLine(fieldSummary.SignerId + ", " + fieldSummary.DocumentId + ", " +
                    fieldSummary.FieldId + ": " + fieldSummary.FieldValue);
            }

            StringBuilder sb = new StringBuilder();
            
            sb.Append("Signer ID, Document ID, Field ID: Value" + System.Environment.NewLine);
            foreach (FieldSummary fieldSummary in fieldSummaries)
            {
                sb.Append(fieldSummary.SignerId + ", " + fieldSummary.DocumentId + ", " +
                    fieldSummary.FieldId + ": " + fieldSummary.FieldValue + System.Environment.NewLine);
            }

            return sb.ToString();
         }
	}
}
