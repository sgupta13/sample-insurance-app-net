# README #

e-SignLive sample insurance application in C# using the .NET framework. This Sample Insurance Application demonstrates how to integrate e-SignLive with a web application portal.

How To Deploy on APPHARBOR

1. Create an [AppHarbor account](https://appharbor.com/user/new)
2. Download an install the [AppHarbor CLI](http://blog.appharbor.com/2012/4/25/introducing-the-appharbor-command-line-utility) 
3. Create a root directory for this project on your computer.
4. Checkout the project using git:

        git clone https://bitbucket.org/sgupta13/sample-insurance-app-net.git

5. Navigate to this project root directory (referred to as $) and login to AppHarbor

        $ appharbor user login
        Username: example
        Password:
        Successfully logged in as example

5. Create a new application on Appharbor

        $ appharbor create insurance
        Created application "insurance" | URL: https://insurance.apphb.com

6. Deploy your code onto the application you just created

        $ appharbor app deploy
        Getting upload credentials..

        Uploading package <100% of 25.1 MB>. 0 seconds left
        The package will be deployed to application "insurance".

        Enter a deployment message: initial commit
        Notifying AppHarbor.
        Deploying...Open application overview with 'appharbor open'.

7. Open your application in a web browser with the following command:

        $ appharbor open

8. Congratulations, your application should now be running on AppHarbor.